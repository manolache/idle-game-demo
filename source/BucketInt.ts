type Observer = (value: string) => void;

export class BucketInt {

    static NumberFormat = new Intl.NumberFormat("en-US");
    static SANITIZE_REGEX = /,|-|[a-z]|\..*$/gi;

    constructor(value: string = "0") {
        this.value = value;
    }

    private buckets: number[] = [0];
    private observers = new Set<Observer>();

    private notify() {
        const value = this.value;
        this.observers.forEach(observer => observer(value));
    }

    private takeOne(number: BucketInt, index: number) {

        let i = index + 1;
        number.buckets[i] -= 1;

        if (number.buckets[i] < 0) {
            this.takeOne(number, i);
            number.buckets[i] = 999;
        }
    }

    private carryOne(number: BucketInt, index: number, amount = 1) {

        let i = index + 1;

        if (number.buckets[i] === undefined) {
            number.buckets.push(amount);
        } else {
            if ((number.buckets[i] + amount) < 1000) {
                number.buckets[i] += amount;
            } else {
                this.carryOne(number, i);
                number.buckets[i] = number.buckets[i] + amount - 1000;
            }
        }
    }

    get value(): string {

        const value = this.buckets.reduceRight((result, number) => {
            return result + String(number).padStart(3, "0");
        }, "");

        return BucketInt.NumberFormat.format(value);
    }

    set value(input: string) {

        if (!input) {
            this.buckets = [0];
            this.notify();
            return;
        }

        const value = input.replace(BucketInt.SANITIZE_REGEX, "");

        if (BucketInt.NumberFormat.format(value) === "NaN") {
            this.buckets = [0];
            this.notify();
            return;
        }

        this.buckets = [];

        for (let i = value.length; i > 0; i -= 3) {

            const number = Number(value.substring(i, i - 3));

            if (Number.isNaN(number)) {
                this.buckets = [0];
                this.notify();
                return;
            }

            this.buckets.push(number);
        }

        this.notify();
    }

    copy(other: BucketInt): BucketInt {
        this.value = other.value;
        return this;
    }

    clone(): BucketInt {
        return new BucketInt(this.value);
    }

    add(number: BucketInt): BucketInt {

        for (let i = 0; i < number.buckets.length; i++) {

            const result = (this.buckets[i] ?? 0) + number.buckets[i];

            if (result < 1000) {
                this.buckets[i] = result;
                continue;
            }

            this.carryOne(this, i);
            this.buckets[i] = result - 1000;
        }

        this.notify();
        return this;
    }

    sub(other: BucketInt): BucketInt {

        if (this.buckets.length === 1 && this.buckets[0] === 0) {
            return this;
        }

        if (this.isLessThan(other)) {
            this.value = "0";
            return this;
        }

        let result;

        for (let i = 0; i < this.buckets.length; i++) {

            result = this.buckets[i] - (other.buckets[i] ?? 0);

            if (result < 0) {
                this.takeOne(this, i);
                this.buckets[i] = result + 1000;
                continue;
            }

            this.buckets[i] = result;
        }

        for (let i = this.buckets.length - 1; i >= 0; i--) {

            if (
                this.buckets.length === 1 ||
                this.buckets[this.buckets.length - 1] !== 0
            ) break;

            this.buckets.pop();
        }

        this.notify();
        return this;
    }

    mul(other: BucketInt): BucketInt {

        if (
            this.buckets[this.buckets.length - 1]   === 0 ||
            other.buckets[other.buckets.length - 1] === 1 &&
            other.buckets.length === 1
        ) {
            return this;
        }

        if (this.buckets[other.buckets.length - 1] === 1) {
            this.value = other.value;
            return this;
        }

        if (other.buckets[other.buckets.length - 1] === 0) {
            this.value = "0";
            return this;
        }

        const result = new BucketInt();
        const iterations = this.buckets.length;

        for (let i = 0; i < iterations; i++) {
            for (let j = 0; j < other.buckets.length; j++) {

                const r = this.buckets[i] * other.buckets[j];

                if (result.buckets[i + j] === undefined) {
                    result.buckets[i + j] = r % 1000;
                } else {
                    if (result.buckets[i + j] + r % 1000 < 1000) {
                        result.buckets[i + j] += r % 1000;
                    } else {
                        const x = result.buckets[i + j] + r % 1000 - 1000;
                        this.carryOne(result, i + j);
                        result.buckets[i + j] = x;
                    }
                }

                const carry = Math.trunc(r / 1000);
                if (carry > 0) {
                    this.carryOne(result, i + j, carry);
                }
            }
        }

        this.value = result.value;
        return this;
    }

    div(other: BucketInt): BucketInt {

        if (
            this.buckets[this.buckets.length - 1]   === 0 ||
            other.buckets[other.buckets.length - 1] === 0 ||
            other.buckets[other.buckets.length - 1] === 1 &&
            other.buckets.length === 1
        ) {
            return this;
        }

        const remainder = new BucketInt();
        const iterations = this.buckets.length - 1;

        for (let i = iterations; i >= 0; i--) {

            if (i === iterations) {
                remainder.buckets[0] = this.buckets[i];
            } else {
                remainder.buckets.unshift(this.buckets[i]);
            }

            this.buckets[i] = 0;

            while(other.isLessThan(remainder) || other.isEqualTo(remainder)) {
                this.buckets[i] += 1;
                remainder.sub(other);
            }
        }

        for (let i = this.buckets.length - 1; i >= 0; i--) {

            if (
                this.buckets.length === 1 ||
                this.buckets[this.buckets.length - 1] !== 0
            ) break;

            this.buckets.pop();
        }

        this.notify();
        return this;
    }

    scale(factor: number): BucketInt {

        if (factor === 0) {
            this.value = "0";
            return this;
        }

        if (factor === 1) {
            return this;
        }

        factor = Math.trunc(factor * 100);

        let carry = 0;
        let result;

        const iterations = this.buckets.length;

        for (let i = 0; i < iterations; i++) {

            result = this.buckets[i] * factor + carry;

            carry = Math.trunc(result / 1000);
            this.buckets[i] = result % 1000;

            if (i === iterations - 1) {

                if (carry < 1000) {
                    this.buckets.push(carry);
                    break;
                }

                const value = String(carry);

                for (let i = value.length; i > 0; i -= 3) {
                    this.buckets.push(Number(value.substring(i, i - 3)));
                }
            }
        }

        this.value = this.value.slice(0, -2);

        return this;
    }

    isEqualTo(number: BucketInt): boolean {

        if (this.buckets.length !== number.buckets.length) {
            return false;
        }

        for (let i = this.buckets.length - 1; i >= 0; i--) {
            if (this.buckets[i] !== number.buckets[i]) return false;
        }

        return true;
    }

    isLessThan(other: BucketInt): boolean {

        if (this.isEqualTo(other)) {
            return false;
        }

        if (this.buckets.length > other.buckets.length) {
            return false;
        }

        if (this.buckets.length === other.buckets.length) {
            for (let i = this.buckets.length - 1; i >= 0; i--) {
                if (this.buckets[i] < other.buckets[i]) return true;
                if (this.buckets[i] > other.buckets[i]) return false;
            }
        }

        return true;
    }

    isGreaterThan(other: BucketInt): boolean {

        if (this.isEqualTo(other)) {
            return false;
        }

        return !this.isLessThan(other);
    }

    addObserver(observer: Observer) {
        observer(this.value);
        this.observers.add(observer);
        return () => this.removeObserver(observer);
    }

    removeObserver(observer: Observer) {
        this.observers.delete(observer);
    }
}
