import {
    vi,
    test,
    expect,
    describe
} from "vitest";

import { BucketInt } from "./BucketInt";

describe("BucketInt", () => {

    test("should return a new instance after calling 'clone'", () => {

        const a = new BucketInt("100");
        const b = a.clone();

        b.value = "101";

        expect(a).not.toBe(b);
        expect(a.value).toBe("100");
        expect(b.value).toBe("101");
    });

    test.each([
        "add", "sub", "mul", "div", "copy"
    ])("should return an instance to itself after calling '%s'", (method) => {

        const a = new BucketInt();
        const b = new BucketInt();

        expect(a[<"add"| "sub"| "mul"| "div" | "copy">method](b)).toBe(a);
    });

    test("should copy the value of another instance when calling 'copy'", () => {
        const a = new BucketInt("100");
        const b = new BucketInt("101");
        expect(b.copy(a).value).toBe("100");
    });

    describe("initial value", () => {

        test("should set to 0 by default", () => {
            const a = new BucketInt();
            expect(a.value).toBe("0");
        });

        test("should set to the value from the constructor", () => {
            const a = new BucketInt("75,025,500,001,010");
            expect(a.value).toBe("75,025,500,001,010");
        });
    });

    describe("value getter", () => {
        test.each`
        input               | output
        ${"105"}            | ${"105"}
        ${"1,015"}          | ${"1,015"}
        ${"5,001,015"}      | ${"5,001,015"}
        ${"5,001,015,000"}  | ${"5,001,015,000"}
        `("$input -> a.value === $output", ({input, output}) => {
            expect(new BucketInt(input).value).toBe(output);
        });
    });

    describe("value setter", () => {
        test.each`
        input               | output
        ${"105"}            | ${"105"}
        ${"1,005"}          | ${"1,005"}
        ${"5,001,015"}      | ${"5,001,015"}
        ${"1,005,001,015"}  | ${"1,005,001,015"}
        `("a.value = $input -> a.value === $output", ({input, output}) => {

            const a = new BucketInt();

            a.value = input;

            expect(a.value).toBe(output);
        });
    });

    describe("comparison", () => {

        test.each`
        x                   | y                     | expected
        ${"0"}              | ${"1"}                | ${true}
        ${"1"}              | ${"1"}                | ${false}
        ${"1,000"}          | ${"100"}              | ${false}
        ${"1,000"}          | ${"1,001"}            | ${true}
        ${"1,000"}          | ${"1,000,000"}        | ${true}
        ${"1,000,000"}      | ${"5,000,000"}        | ${true}
        ${"5,000,001"}      | ${"5,000,000"}        | ${false}
        ${"1,000,000,000"}  | ${"1,000,000,000"}    | ${false}
        `("$x < $y -> $expected", ({x, y, expected}) => {
            const a = new BucketInt(x);
            const b = new BucketInt(y);
            expect(a.isLessThan(b)).toBe(expected);
        });

        test.each`
        x                   | y                     | expected
        ${"0"}              | ${"1"}                | ${false}
        ${"1"}              | ${"1"}                | ${false}
        ${"1,000"}          | ${"100"}              | ${true}
        ${"1,000"}          | ${"1,001"}            | ${false}
        ${"1,000"}          | ${"1,000,000"}        | ${false}
        ${"1,000,000"}      | ${"5,000,000"}        | ${false}
        ${"5,000,001"}      | ${"5,000,000"}        | ${true}
        ${"1,000,000,000"}  | ${"1,000,000,000"}    | ${false}
        `("$x > $y -> $expected", ({x, y, expected}) => {
            const a = new BucketInt(x);
            const b = new BucketInt(y);
            expect(a.isGreaterThan(b)).toBe(expected);
        });

        test.each`
        x                   | y                     | expected
        ${"0"}              | ${"1"}                | ${false}
        ${"1"}              | ${"1"}                | ${true}
        ${"1,000"}          | ${"100"}              | ${false}
        ${"1,000"}          | ${"1,001"}            | ${false}
        ${"1,000"}          | ${"1,000,000"}        | ${false}
        ${"1,000,000"}      | ${"5,000,000"}        | ${false}
        ${"5,000,001"}      | ${"5,000,000"}        | ${false}
        ${"1,000,000,000"}  | ${"1,000,000,000"}    | ${true}
        `("$x === $y -> $expected", ({x, y, expected}) => {
            const a = new BucketInt(x);
            const b = new BucketInt(y);
            expect(a.isEqualTo(b)).toBe(expected);
        });
    });

    describe("observers", () => {

        test("should notify an observer at the time of the subscription", () => {

            const observers = [vi.fn(), vi.fn(), vi.fn()];

            observers.forEach(observer => {
                new BucketInt("1,000,001").addObserver(observer);
                expect(observer).toHaveBeenCalledTimes(1);
                expect(observer).toHaveBeenCalledWith("1,000,001");
            });
        });

        test("should notify the observers when the value has changed", () => {

            const a = new BucketInt();
            const observers = [vi.fn(), vi.fn(), vi.fn()];

            observers.forEach(observer => {
                a.addObserver(observer);
            });

            a.value = "42";

            observers.forEach(observer => {
                expect(observer).toHaveBeenCalledTimes(2);
                expect(observer).toHaveBeenNthCalledWith(2, "42");
            });
        });

        test.each`
        x           | y         | op            | result
        ${"99"}     | ${"1"}    | ${"add"}      | ${"100"}
        ${"101"}    | ${"1"}    | ${"sub"}      | ${"100"}
        ${"1000"}   | ${"10"}   | ${"div"}      | ${"100"}
        ${"10"}     | ${"10"}   | ${"mul"}      | ${"100"}
        ${"50"}     | ${"2"}    | ${"scale"}    | ${"100"}
        `("should notify observers after the $op operation", ({ x, y, op, result }) => {

            const observers = [vi.fn(), vi.fn(), vi.fn()];
            const a = new BucketInt(x);

            observers.forEach(observer => {
                a.addObserver(observer);
            });

            if (op === "scale") {
                a.scale(y);
            } else {
                a[<"add" | "sub" | "mul" | "div">op](new BucketInt(y));
            }

            observers.forEach(observer => {
                expect(observer).toHaveBeenCalledTimes(2);
                expect(observer).toHaveBeenNthCalledWith(2, result);
            });
        });

        test("should remove an observer when calling 'removeObserver'", () => {

            const a = new BucketInt();
            const observers = [vi.fn(), vi.fn(), vi.fn()];

            observers.forEach(observer => {
                a.addObserver(observer);
            });

            a.removeObserver(observers[1]);
            a.value = "100";

            observers.forEach((observer, i) => {
                if (i === 1) {
                    expect(observer).toHaveBeenCalledTimes(1);
                    expect(observer).toHaveBeenNthCalledWith(1, "0");
                } else {
                    expect(observer).toHaveBeenCalledTimes(2);
                    expect(observer).toHaveBeenNthCalledWith(2, "100");
                }
            });
        });

        test("should return a subscription handle after calling 'addObserver'", () => {

            const a = new BucketInt();
            const observers = [vi.fn(), vi.fn(), vi.fn()];

            const handles = observers.map(observer => {
                return a.addObserver(observer);
            });

            handles[1]();
            a.value = "100";

            observers.forEach((observer, i) => {
                if (i === 1) {
                    expect(observer).toHaveBeenCalledTimes(1);
                    expect(observer).toHaveBeenNthCalledWith(1, "0");
                } else {
                    expect(observer).toHaveBeenCalledTimes(2);
                    expect(observer).toHaveBeenNthCalledWith(2, "100");
                }
            });
        });
    });

    describe("scaling", () => {
        test.each`
        x                               | factor                | result
        ${"10"}                         | ${0}                  | ${"0"}
        ${"10"}                         | ${1}                  | ${"10"}
        ${"0"}                          | ${10}                 | ${"0"}
        ${"1"}                          | ${10}                 | ${"10"}
        ${"100"}                        | ${1.1}                | ${"110"}
        ${"500"}                        | ${2.5}                | ${"1,250"}
        ${"1,000"}                      | ${1.1}                | ${"1,100"}
        ${"2,999"}                      | ${2.99}               | ${"8,967"}
        ${"999,999"}                    | ${2.25}               | ${"2,249,997"}
        ${"2,999,999"}                  | ${2.99}               | ${"8,969,997"}
        ${"999,999,999"}                | ${2.9}                | ${"2,899,999,997"}
        ${"999,999,999"}                | ${2.99}               | ${"2,989,999,997"}
        ${"999,999,999"}                | ${999_999_999.99}     | ${"999,999,998,990,000,000"}
        ${"999,999,999,999,999,999"}    | ${1_000_000_000}      | ${"999,999,999,999,999,999,000,000,000"}
        ${"1,000,000,000,000,000,000"}  | ${1_000_000_000}      | ${"1,000,000,000,000,000,000,000,000,000"}
        `("$x scaled by $factor -> $result", ({x, factor, result}) => {
            expect(new BucketInt(x).scale(factor).value).toBe(result);
        });
    });

    describe("addition", () => {

        test("should not mutate the other operand", () => {
            const a = new BucketInt("90");
            const b = new BucketInt("10");

            a.add(b);

            expect(a.value).toBe("100");
            expect(b.value).toBe("10");
        });

        test.each`
        x                       | y                     | result
        ${"9"}                  | ${"1"}                | ${"10"}
        ${"1"}                  | ${"9"}                | ${"10"}
        ${"10"}                 | ${"0"}                | ${"10"}
        ${"0"}                  | ${"10"}               | ${"10"}
        ${"999"}                | ${"1"}                | ${"1,000"}
        ${"998,999"}            | ${"3,001"}            | ${"1,002,000"}
        ${"1,999,999"}          | ${"1"}                | ${"2,000,000"}
        ${"5,123,005"}          | ${"2,001"}            | ${"5,125,006"}
        ${"3,900,005"}          | ${"8,005,100,002"}    | ${"8,009,000,007"}
        ${"190,824,543,970"}    | ${"809,175,456,030"}  | ${"1,000,000,000,000"}
        `("$x + $y -> $result", ({x, y, result}) => {

            const a = new BucketInt();
            const b = new BucketInt();

            a.value = x;
            b.value = y;

            expect(a.add(b).value).toBe(result);
        });
    });

    describe("subtraction", () => {

        test("should not mutate the other operand", () => {
            const a = new BucketInt("110");
            const b = new BucketInt("10");

            a.sub(b);

            expect(a.value).toBe("100");
            expect(b.value).toBe("10");
        });

        test.each`
        x                   | y                 | result
        ${"10"}             | ${"0"}            | ${"10"}
        ${"0"}              | ${"10"}           | ${"0"}
        ${"565"}            | ${"245"}          | ${"320"}
        ${"1,200"}          | ${"120"}          | ${"1,080"}
        ${"2,000"}          | ${"5"}            | ${"1,995"}
        ${"1,200"}          | ${"201"}          | ${"999"}
        ${"1,001"}          | ${"1,000"}        | ${"1"}
        ${"3,200"}          | ${"700"}          | ${"2,500"}
        ${"3,200"}          | ${"2,700"}        | ${"500"}
        ${"1,010,200"}      | ${"1,120"}        | ${"1,009,080"}
        ${"5,750,200"}      | ${"3,500,700"}    | ${"2,249,500"}
        `("$x - $y -> $result", ({x, y, result}) => {

            const a = new BucketInt();
            const b = new BucketInt();

            a.value = x;
            b.value = y;

            expect(a.sub(b).value).toBe(result);
        });
    });

    describe("multiplication", () => {

        test("should not mutate the other operand", () => {
            const a = new BucketInt("10");
            const b = new BucketInt("10");

            a.mul(b);

            expect(a.value).toBe("100");
            expect(b.value).toBe("10");
        });

        test.each`
        x                                   | y                     | result
        ${"0"}                              | ${"10"}               | ${"0"}
        ${"1"}                              | ${"10"}               | ${"10"}
        ${"10"}                             | ${"0"}                | ${"0"}
        ${"10"}                             | ${"1"}                | ${"10"}
        ${"10,000"}                         | ${"3"}                | ${"30,000"}
        ${"10,100"}                         | ${"3,999"}            | ${"40,389,900"}
        ${"10,100"}                         | ${"133,999"}          | ${"1,353,389,900"}
        ${"999,100"}                        | ${"999,999"}          | ${"999,099,000,900"}
        ${"10,000,100"}                     | ${"1,003,999"}        | ${"10,040,090,399,900"}
        ${"999,999,999,999,999,999,999"}    | ${"999,999,999,999"}  | ${"999,999,999,998,999,999,999,000,000,000,001"}
        `("$x * $y -> $result", ({x, y, result}) => {

            const a = new BucketInt();
            const b = new BucketInt();

            a.value = x;
            b.value = y;

            expect(a.mul(b).value).toBe(result);
        });
    });

    describe("division", () => {

        test("should not mutate the other operand", () => {
            const a = new BucketInt("100");
            const b = new BucketInt("10");

            a.div(b);

            expect(a.value).toBe("10");
            expect(b.value).toBe("10");
        });

        test.each`
        x                       | y                     | result
        ${"0"}                  | ${"10"}               | ${"0"}
        ${"10"}                 | ${"0"}                | ${"10"}
        ${"10"}                 | ${"1"}                | ${"10"}
        ${"520"}                | ${"2"}                | ${"260"}
        ${"1,520"}              | ${"2"}                | ${"760"}
        ${"100,520"}            | ${"2"}                | ${"50,260"}
        ${"1,100,520"}          | ${"2"}                | ${"550,260"}
        ${"1,000,100,520"}      | ${"2"}                | ${"500,050,260"}
        ${"1,000,100,520"}      | ${"78"}               | ${"12,821,801"}
        ${"1,000,100,520"}      | ${"178"}              | ${"5,618,542"}
        ${"1,000,100,520"}      | ${"5,178"}            | ${"193,144"}
        ${"100,000,000"}        | ${"1,000,000"}        | ${"100"}
        `("$x / $y -> $result", ({x, y, result}) => {

            const a = new BucketInt();
            const b = new BucketInt();

            a.value = x;
            b.value = y;

            expect(a.div(b).value).toBe(result);
        });
    });
});
