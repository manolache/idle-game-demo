import { BucketInt } from "./BucketInt";

const $display      = <HTMLDivElement>document.querySelector("#display");
const $addButton    = <HTMLButtonElement>document.querySelector("#add");
const $info         = <HTMLDivElement>document.querySelector("#info");

const $buyButton        = <HTMLButtonElement>document.querySelector("#buy");
const $buyButtonCost    = <HTMLButtonElement>document.querySelector("#buy>.cost");
const $buyButtonLabel   = <HTMLButtonElement>document.querySelector("#buy>.label");

const $buyMaxButton         = <HTMLButtonElement>document.querySelector("#buy-max");
const $buyMaxButtonCost     = <HTMLButtonElement>document.querySelector("#buy-max>.cost");
const $buyMaxButtonLabel    = <HTMLButtonElement>document.querySelector("#buy-max>.label");

const playerBank            = new BucketInt();
const playerValueGenerator  = new BucketInt("100,000,000");

const idleGeneratorCost         = new BucketInt("1,000,000");
const idleValueGenerator        = new BucketInt("250,000,000,000");
const idleValueGeneratorCount   = new BucketInt();

function updateGui() {

    $info.innerText = `Adds ${idleValueGeneratorCount.value}×$${idleValueGenerator.value}/s`;
    
    $buyButtonLabel.innerText   = `Buy × 1`;
    $buyButtonCost.innerText    = `Cost: $${idleGeneratorCost.value}`;

    const amount = playerBank.clone().div(idleGeneratorCost);

    $buyMaxButtonLabel.innerText    = `Buy Max (× ${amount.value})`;
    $buyMaxButtonCost.innerText     = `Cost: $${idleGeneratorCost.clone().mul(amount).value}`;
}

updateGui();

playerBank.addObserver(value => {
    $display.innerText = `$${value}`;
    updateGui();
});

$addButton?.addEventListener("click", () => {
    playerBank.add(playerValueGenerator);
});

$buyButton?.addEventListener("click", () => {

    if (idleGeneratorCost.isGreaterThan(playerBank)) {
        return;
    }

    playerBank.sub(idleGeneratorCost);

    idleGeneratorCost.scale(1.05);
    idleValueGeneratorCount.add(new BucketInt("1"));

    updateGui();
});

$buyMaxButton?.addEventListener("click", () => {

    if (idleGeneratorCost.isGreaterThan(playerBank)) {
        return;
    }

    const amount = playerBank.clone().div(idleGeneratorCost);

    playerBank.sub(idleGeneratorCost.clone().mul(amount));

    idleValueGeneratorCount.add(amount);
    idleGeneratorCost.mul(amount.scale(1.05));

    updateGui();
});

setInterval(() => {

    if (idleValueGeneratorCount.value === "0") {
        return;
    }

    playerBank.add(idleValueGenerator.clone().mul(idleValueGeneratorCount));
}, 1000);
